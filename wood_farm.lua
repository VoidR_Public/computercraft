local args = { ... }

local rows = tonumber(args[1]) or 0
local columns = tonumber(args[2]) or 0

function  cutDownTree ()
    local upCount = 0
    turtle.dig()
    turtle.forward()
    while isTreeUp() do
        turtle.digUp()
        turtle.up()
        upCount = upCount + 1;
    end

    while (upCount > 0) do
        if turtle.down() then
            upCount = upCount - 1;
        else
            turtle.digDown()
        end
    end
    turtle.back()
end

function isTreeUp()
    local has_block, data = turtle.inspectUp()

    if has_block then
        if data.name == "minecraft:birch_log" then
            return true;
        end
    end

    return false
end


function isTree()
    local has_block, data = turtle.inspect()

    if has_block then
        if data.name == "minecraft:birch_log" then
            return true;
        end
    end

    return false
end


function cutDownIfTreeAndPlant()
    if isTree() then
        cutDownTree()
    end
    turtle.place()
end


function moveToNextTree()
    turtle.suck()
    turtle.forward()
    turtle.suck()
    turtle.forward()
end

function checkSurrounding()  
    turtle.turnRight()
    turtle.suck()
    cutDownIfTreeAndPlant()
    turtle.turnLeft()
    turtle.suck()
    turtle.turnLeft()
    turtle.suck()
    cutDownIfTreeAndPlant()
    turtle.turnRight()
end


function checkRow(amount)
    for i = 1, amount -1, 1 do
        checkSurrounding()
        moveToNextTree()
    end
    checkSurrounding()
end

function moveToNextRow(isRightNext)
    turtle.forward()
    if isRightNext then
        turtle.turnRight()
    else
        turtle.turnLeft()
    end
    turtle.forward()
    moveToNextTree()
    turtle.forward()
    if isRightNext then
        turtle.turnRight()
    else
        turtle.turnLeft()
    end
    turtle.forward()
end 

function checkWholeFarm(rowCount, columCount)
    local isRight = false;
    checkRow(rowCount);
    for i = 1, columCount - 1, 1 do
        moveToNextRow(isRight)
        isRight = not isRight
        checkRow(rowCount);
    end
    goToStart()
end

function shouldRefuel()
    return rows * columns * 4 > turtle.getFuelLevel()
end

function dropToChest(startIndex, endIndex)
    -- Add test if infront of chest
    for i = startIndex, endIndex, 1 do
        turtle.select(i);
        turtle.drop();
    end
    turtle.select(1)

end

function goToStart()  
    if (columns % 2 ~= 0) then
        turtle.turnLeft()
        turtle.turnLeft()
        for i = 1, rows - 1, 1 do
            moveToNextTree()
        end
    end

    turtle.forward()
    turtle.turnLeft()
    turtle.forward()
    for i = 1, (columns - 1) * 2 - 1, 1 do
        moveToNextTree()
    end
    turtle.forward()
end

function reset()
    dropToChest(2, 16)
    if shouldRefuel() then
        turtle.turnRight()
        turtle.select(2);
        turtle.suck(64)
        turtle.refuel()
        turtle.select(1)
        turtle.turnLeft()
    end
    turtle.turnLeft()
    turtle.forward()
end


while true do
    checkWholeFarm(rows, columns)
    reset()
end